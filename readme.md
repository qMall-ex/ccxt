## Authentication API
<details open>
  <summary>
section is under development
  </summary>

#### getConnection
<details open>
  <summary>
  </summary>

```java
GET /exchange/qmall
```

```java
curl -X GET "http://localhost:3000/exchange/qmall" -H "accept: application/json"
```

**Response samples**

```json
{
  "id": "string",
  "name": "string",
  "private": true,
  "enableRateLimit": true,
  "countries": [
    "string"
  ],
  "rateLimit": 0,
  "twofa": false,
  "has": {
    "CORS": "true",
    "publicAPI": "true",
    "privateAPI": "true",
    "cancelOrder": "true",
    "cancelOrders": "true",
    "createDepositAddress": "true",
    "createOrder": "true",
    "createMarketOrder": "true",
    "createLimitOrder": "true",
    "editOrder": "true",
    "fetchBalance": "true",
    "fetchBidsAsks": "true",
    "fetchClosedOrders": "true",
    "fetchCurrencies": "true",
    "fetchDepositAddress": "true",
    "fetchFundingFees": "true",
    "fetchL2OrderBook": "true",
    "fetchMarkets": "true",
    "fetchMyTrades": "true",
    "fetchOHLCV": "true",
    "fetchOpenOrders": "true",
    "fetchOrder": "true",
    "fetchOrderBook": "true",
    "fetchOrderBooks": "true",
    "fetchOrders": "true",
    "fetchTicker": "true",
    "fetchTickers": "true",
    "fetchTrades": "true",
    "fetchTradingFees": "true",
    "fetchTradingLimits": "true",
    "withdraw": "true"
  },
  "urls": {}
}
```

**Responses**

- $`\textcolor{green}{\bold{200}}`$ - **Success**
- $`\textcolor{red}{\bold{404}}`$ - **Exchange with that name is NOT supported**
- $`\textcolor{red}{\bold{500}}`$ - **If an unexpected error occurred**


</details>

</details>

## Exchange Management API

## Public Data API
<details open>
  <summary>
APIs that retrieve public data (like ticker, order books, trades, etc)
  </summary>

#### Markets
<details open>
<summary>
  </summary>

```java
GET /exchange/qmall/markets
```

```java
curl -X GET "http://localhost:3000/exchange/qmall/markets" -H "accept: application/json"
```

**Response samples**

```json
[
    {
        "id": "bcc_btc",
        "symbol": "BCH/BTC",
        "base": "BCH",
        "quote": "BTC",
        "limits": {
            "amount": {},
            "price": {},
            "cost": {}
        },
        "precision": {
            "amount": 8,
            "price": 8
        }
    },
{ ... }  
]
```

**Responses code**
![2022-04-10_12-40](uploads/824ad472daccbf83763a19ce4a786991/2022-04-10_12-40.png)

</details>

---

#### Order book
<details open>
<summary>
  </summary>

```java
GET /exchange/qmall/orderBook
```

```java
curl -X GET "http://localhost:3000/exchange/qmall/orderBook?symbol=BTC%2FUSDT&limit=10" -H "accept: application/json"
```


QUERY PARAMETERS:
| | | |
| :---: | :---: | :---: |
| **symbol** (*required*)| String | required Unified CCXT symbol (e.g. `"BTC/USDT"`) |
| **limit** | Integer | The number of orders to return in the order book (e.g. `10`) |

**Response samples:**

```java
{
  "bids": [
    {
      "price": 42662.11,
      "amount": 10.57833
    },
    {
      ...
    }
  ],
  "asks": [
    {
      "price": 42662.12,
      "amount": 0.60128
    },
    {
      ...
    }
  ]
}
```

**Responses code**
![2022-04-10_12-40](uploads/824ad472daccbf83763a19ce4a786991/2022-04-10_12-40.png)

</details>

---

#### Order book (vers.L2)
<details open>
<summary>
  </summary>

</details>

---

#### Trades
<details open>
<summary>
  </summary>

```java
GET /exchange/qmall/trades
```

```java
curl -X GET "http://localhost:3000/exchange/qmall/trades?symbol=BTC%2FUSDT&since=10&limit=10" -H "accept: application/json"
```

**QUERY PARAMETERS:**
|  |  |  |
| :----: | :----: | :----: |
| **symbol** (*required*) | string | The symbol of the exchange's data to be retrieved.  (e.g. `"BTC/USDT"`)|
| **since** | string | Retrieve the trades starting from 'since' |
| **limit** | number | The limit of the exchange's trades to be retrieved |

**Responce samples:**
```java
[
  {
    "id": "1563137616",
    "info": {
      "price": 42775.04,
      "amount": 0.0074,
      "time": 1649573647536,
      "direction": 2,
      "tid": "1563137616"
    },
    "timestamp": 1649573647536,
    "datetime": "2022-04-10T06:54:07.536Z",
    "symbol": "BTC/USDT",
    "side": "sell",
    "price": 42775.04,
    "amount": 0.0074
  },
  {
    ...
  }
]
```

**Responses code**
![2022-04-10_12-40](uploads/824ad472daccbf83763a19ce4a786991/2022-04-10_12-40.png)

</details>

---

#### Ticker
<details open>
<summary>
  </summary>

```java
GET /exchange/qmall/ticker
```

```java
curl -X GET "http://localhost:3000/exchange/qmall/ticker?symbol=ETH%2FBTC" -H "accept: application/json"
```

**QUERY PARAMETERS:**
|  |  |  |
| :----: | :----: | :----: |
| **symbol** (*required*) | string | The symbol of the exchange's data to be retrieved.  (e.g. `"BTC/USDT"`)|

**Responce samples:**
```java
{
  "symbol": "ETH/BTC",
  "timestamp": 1649575933835,
  "datetime": "2022-04-10T07:32:13.835Z",
  "high": 0.07647953,
  "low": 0.07559726,
  "bid": 0.07594091,
  "ask": 0.07605663,
  "close": 0.07608045,
  "last": 0.07608045,
  "baseVolume": 4.0801,
  "info": {
    "sell": "0.07605663",
    "buy": "0.07594091",
    "open": "0.07559726",
    "high": "0.07647953",
    "low": "0.07559726",
    "last": "0.07608045",
    "vol": "4.0801",
    "timestamp": 1649575933835
  }
}
```

**Responses code**
![2022-04-10_12-40](uploads/824ad472daccbf83763a19ce4a786991/2022-04-10_12-40.png)


</details>

---

#### Tickers
<details open>
<summary>
A price ticker contains statistics for a particular market/symbol for some period of time in recent past, usually last 24 hours. The methods for fetching tickers are described below.
  </summary>

```java
GET /exchange/qmall/tickers
```

```java
curl -X GET "http://localhost:3000/exchange/qmall/tickers?symbol=BTC%2FUSDT" -H "accept: application/json"
```

```java
curl -X GET "http://localhost:3000/exchange/qmall/tickers?symbol=BTC%2FUSDT%2CETH%2FBTC" -H "accept: application/json"
```

**QUERY PARAMETERS:**
|  |  |  |
| :----: | :----: | :----: |
| **symbol** (*required*) v.1| string | The symbol of the exchange's data to be retrieved.  (e.g. `"BTC/USDT"`)|
| **symbol** (*required*) v.2| string | The symbol of the exchange's data to be retrieved.  (e.g. `BTC/USDT,ETH/BTC`)|

**Responce samples:**
<details open>
  <summary>
with one parameter
  </summary>

```java
[
  {
    "symbol": "BTC/USDT",
    "timestamp": 1649576705966,
    "datetime": "2022-04-10T07:45:05.966Z",
    "high": 42896.64,
    "low": 42158.67,
    "bid": 42622.65,
    "ask": 42622.66,
    "vwap": 42537.84923083,
    "close": 42622.65,
    "last": 42622.65,
    "baseVolume": 15112.80589,
    "quoteVolume": 642866258.4036233,
    "info": {
      "symbol": "BTCUSDT",
      "priceChange": "213.46000000",
      "priceChangePercent": "0.503",
      "weightedAvgPrice": "42537.84923083",
      "prevClosePrice": "42408.45000000",
      "lastPrice": "42622.65000000",
      "lastQty": "0.00762000",
      "bidPrice": "42622.65000000",
      "bidQty": "2.69899000",
      "askPrice": "42622.66000000",
      "askQty": "10.69687000",
      "openPrice": "42409.19000000",
      "highPrice": "42896.64000000",
      "lowPrice": "42158.67000000",
      "volume": "15112.80589000",
      "quoteVolume": "642866258.40362330",
      "openTime": 1649490305966,
      "closeTime": 1649576705966,
      "firstId": 1320898415,
      "lastId": 1321462159,
      "count": 563745
    }
  }
]
```

</details>

<details open>
  <summary>
with two parameter 
  </summary>

```java
[
  {
    "symbol": "BTC/USDT",
    "timestamp": 1649577127409,
    "datetime": "2022-04-10T07:52:07.409Z",
    "high": 42896.64,
    "low": 42158.67,
    "bid": 42649.97,
    "ask": 42649.98,
    "vwap": 42538.49819211,
    "close": 42649.97,
    "last": 42649.97,
    "baseVolume": 15041.79463,
    "quoteVolume": 639855353.6742948,
    "info": {
      "symbol": "BTCUSDT",
      "priceChange": "170.87000000",
      "priceChangePercent": "0.402",
      "weightedAvgPrice": "42538.49819211",
      "prevClosePrice": "42479.10000000",
      "lastPrice": "42649.97000000",
      "lastQty": "0.00456000",
      "bidPrice": "42649.97000000",
      "bidQty": "1.91708000",
      "askPrice": "42649.98000000",
      "askQty": "12.56215000",
      "openPrice": "42479.10000000",
      "highPrice": "42896.64000000",
      "lowPrice": "42158.67000000",
      "volume": "15041.79463000",
      "quoteVolume": "639855353.67429480",
      "openTime": 1649490727409,
      "closeTime": 1649577127409,
      "firstId": 1320901880,
      "lastId": 1321463793,
      "count": 561914
    }
  },
  {
    "symbol": "ETH/BTC",
    "timestamp": 1649577126529,
    "datetime": "2022-04-10T07:52:06.529Z",
    "high": 0.07658,
    "low": 0.075432,
    "bid": 0.076092,
    "ask": 0.076093,
    "vwap": 0.07600232,
    "close": 0.076093,
    "last": 0.076093,
    "baseVolume": 36442.9641,
    "quoteVolume": 2769.74979819,
    "info": {
      "symbol": "ETHBTC",
      "priceChange": "0.00053800",
      "priceChangePercent": "0.712",
      "weightedAvgPrice": "0.07600232",
      "prevClosePrice": "0.07555400",
      "lastPrice": "0.07609300",
      "lastQty": "0.94840000",
      "bidPrice": "0.07609200",
      "bidQty": "25.50930000",
      "askPrice": "0.07609300",
      "askQty": "16.49190000",
      "openPrice": "0.07555500",
      "highPrice": "0.07658000",
      "lowPrice": "0.07543200",
      "volume": "36442.96410000",
      "quoteVolume": "2769.74979819",
      "openTime": 1649490726529,
      "closeTime": 1649577126529,
      "firstId": 334102129,
      "lastId": 334186041,
      "count": 83913
    }
  }
]
```
</details>


**Responses code**
![2022-04-10_12-40](uploads/824ad472daccbf83763a19ce4a786991/2022-04-10_12-40.png)

</details>

---

</details>

## Private Data API
<details open>
  <summary>
In order to be able to access your user account, perform algorithmic trading by placing market and limit orders, query balances, deposit and withdraw funds and so on, you need to obtain your API keys for authentication from each exchange you want to trade with. They usually have it available on a separate tab or page within your user account settings. API keys are exchange-specific and cannnot be interchanged under any circumstances.
  </summary>

#### Balances
<details open>
  <summary>
  </summary>

```java
GET /exchange/qmall/balances
```

```java
curl -X GET "http://localhost:3000/exchange/qmall/balances" -H "accept: application/json"
```

**Responce samples:**
```java
{
  "info": {},
  "balances": [
    {
      "currency": "string",
      "free": 0,
      "used": 0,
      "total": 0
    }
  ]
}
```

![2022-04-10_12-40](uploads/824ad472daccbf83763a19ce4a786991/2022-04-10_12-40.png)

</details>

---

#### Fetch orders
<details open>
  <summary>
  </summary>

```java
GET /exchange/qmall/orders
```

```java
curl -X GET "http://localhost:3000/exchange/qmall/orders?symbol=ETH%2FBUSD&limit=10" -H "accept: application/json"
```

**QUERY PARAMETERS:**
|  |  |  |
| :----: | :----: | :----: |
| **symbol** | string | The symbol of the exchange's data to be retrieved.  (e.g. `"BTC/USDT"`)|
| **since** | string | The symbol of the exchange's data to be retrieved |
| **limit** | number | The limit of the exchange's trades to be retrieved |

**Responce samples:**
```java
[
  {
    "id": "string",
    "timestamp": 0,
    "datetime": "2022-04-10T08:36:59.944Z",
    "symbol": "string",
    "type": "market",
    "side": "buy",
    "price": 0,
    "amount": 0,
    "cost": 0,
    "filled": 0,
    "remaining": 0,
    "status": "open",
    "info": {}
  }
]
```

**Responses code**
![2022-04-10_12-40](uploads/824ad472daccbf83763a19ce4a786991/2022-04-10_12-40.png)



</details>

---

#### Fetch open orders
<details open>
  <summary>
  </summary>

```java
GET /exchange/qmall/orders/open
```

```java
curl -X GET "http://localhost:3000/exchange/qmall/orders/open?symbol=BTC%2FUSDT" -H "accept: application/json"
```
**QUERY PARAMETERS:**
|  |  |  |
| :----: | :----: | :----: |
| symbol | string |The symbol of the exchange's data to be retrieved |
| since | string | Retrieve the orders starting from 'since' |
| limit | number | The limit of the exchange's orders to be retrieved|

**Responce samples:**
```java
[
  {
    "id": "string",
    "timestamp": 0,
    "datetime": "2022-04-10T09:22:55.329Z",
    "symbol": "string",
    "type": "market",
    "side": "buy",
    "price": 0,
    "amount": 0,
    "cost": 0,
    "filled": 0,
    "remaining": 0,
    "status": "open",
    "info": {}
  }
]
```

**Responses code**
![2022-04-10_12-40](uploads/824ad472daccbf83763a19ce4a786991/2022-04-10_12-40.png)

</details>

---

#### Fetch closed orders
<details open>
  <summary>
  </summary>

```java
GET /exchange/qmall/orders/closed
```

```java
curl -X GET "http://localhost:3000/exchange/qmall/orders/closed?symbol=BTC%2FUSDT" -H "accept: application/json"
```

**QUERY PARAMETERS:**
|  |  |  |
| :----: | :----: | :----: |
| symbol | string |The symbol of the exchange's data to be retrieved |
| since | string | Retrieve the orders starting from 'since' |
| limit | number | The limit of the exchange's orders to be retrieved|

**Responce samples:**
```java
[
  {
    "id": "string",
    "timestamp": 0,
    "datetime": "2022-04-10T09:28:41.779Z",
    "symbol": "string",
    "type": "market",
    "side": "buy",
    "price": 0,
    "amount": 0,
    "cost": 0,
    "filled": 0,
    "remaining": 0,
    "status": "open",
    "info": {}
  }
]
```

**Responses code**
![2022-04-10_12-40](uploads/824ad472daccbf83763a19ce4a786991/2022-04-10_12-40.png)

</details>

---

#### Fetch my trades
<details open>
  <summary>
  </summary>

```java
GET /exchange/qmall/trades/mine
```

```java
curl -X GET "http://localhost:3000/exchange/qmall/trades/mine?symbol=ETH%2FBTC" -H "accept: application/json"
```
**QUERY PARAMETERS:**
|  |  |  |
| :----: | :----: | :----: |
| symbol | string |The symbol of the exchange's data to be retrieved |
| since | string | Retrieve the orders starting from 'since' |
| limit | number | The limit of the exchange's orders to be retrieved|

**Responce samples:**
```java
[
  {
    "id": "string",
    "info": {},
    "timestamp": 0,
    "symbol": "string",
    "side": "buy",
    "price": 0,
    "amount": 0
  }
]
```

**Responses code**
![2022-04-10_12-40](uploads/824ad472daccbf83763a19ce4a786991/2022-04-10_12-40.png)

</details>

---

#### Create order
<details open>
  <summary>
  </summary>

```java
POST /exchange/qmall/order
```

Request body
```java
{
  "symbol": "string",
  "type": "market",
  "side": "buy",
  "amount": 0,
  "price": 0,
  "exchangeSpecificParams": {}
}
```
```java
curl -X POST "http://localhost:3000/exchange/qmall/order" -H "accept: application/json" -H "Content-Type: application/json" -d "{\"symbol\":\"string\",\"type\":\"market\",\"side\":\"buy\",\"amount\":0,\"price\":0,\"exchangeSpecificParams\":{}}"
```

**Responce samples:**
```java
{
  "id": "string",
  "timestamp": 0,
  "datetime": "2022-04-10T09:53:08.776Z",
  "symbol": "string",
  "type": "market",
  "side": "buy",
  "price": 0,
  "amount": 0,
  "cost": 0,
  "filled": 0,
  "remaining": 0,
  "status": "open",
  "info": {}
}
```

**Responses code**
![2022-04-10_12-40](uploads/824ad472daccbf83763a19ce4a786991/2022-04-10_12-40.png)


</details>

---

#### Fetch order
<details open>
  <summary>
  </summary>

```java
GET /exchange/qmall/order/{orderId}
```

**QUERY PARAMETERS:**
|  |  |  |
| :----: | :----: | :----: |
|orderId (path)| string	| The id of the order |
| symbol | string |The symbol of the exchange's data to be retrieved |

```java
curl -X GET "http://localhost:3000/exchange/qmall/order/1234?symbol=BTC%2FUSDT" -H "accept: application/json"
```

**Responce samples:**
```java
{
  "id": "string",
  "timestamp": 0,
  "datetime": "2022-04-10T10:00:17.743Z",
  "symbol": "string",
  "type": "market",
  "side": "buy",
  "price": 0,
  "amount": 0,
  "cost": 0,
  "filled": 0,
  "remaining": 0,
  "status": "open",
  "info": {}
}
```

**Responses code**
![2022-04-10_12-40](uploads/824ad472daccbf83763a19ce4a786991/2022-04-10_12-40.png)

</details>

---

#### Cancel order
<details open>
  <summary>
  </summary>

```java
DELETE /exchange/qmall/order/{orderId}
```
**QUERY PARAMETERS:**
|  |  |  |
| :----: | :----: | :----: |
|orderId (path)| string	| The id of the order |
| symbol | string |The symbol of the exchange's data to be retrieved |

```java
curl -X DELETE "http://localhost:3000/exchange/qmall/order/1234?symbol=ETH%2FBTC" -H "accept: application/json"
```

**Responce samples:**
```java
{
  "id": "string",
  "timestamp": 0,
  "datetime": "2022-04-10T10:04:20.603Z",
  "symbol": "string",
  "type": "market",
  "side": "buy",
  "price": 0,
  "amount": 0,
  "cost": 0,
  "filled": 0,
  "remaining": 0,
  "status": "open",
  "info": {}
}
```

**Responses code**
![2022-04-10_12-40](uploads/824ad472daccbf83763a19ce4a786991/2022-04-10_12-40.png)

</details>

---


</details>
